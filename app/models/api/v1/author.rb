class Api::V1::Author < ApplicationRecord
  has_many :books, dependent: :destroy
  has_many :publishers, through: :books

  def self.all_with_books_in_desc_order
    self.order(:last_name).group([:id,:last_name]).map{|author|
      author.books.order(title: :desc).map.with_index{|book, index| 
        {
          book_num: author.books.count - index ,
          Author:"#{author.first_name} #{author.last_name}",
          Title: book.title,
          Description: book.description,
          ISBN: book.isbn 
        }
      }
    }.reject!{|e| e.empty?  }
  end


end
