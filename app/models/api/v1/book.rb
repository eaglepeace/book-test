class Api::V1::Book < ApplicationRecord
  belongs_to :author
  belongs_to :publisher
  validates :author_id, presence: true
  validates :publisher_id, presence: true

  def author_full_name
    "#{self.author.first_name} #{self.author.last_name}"
  end

  def author_email
    "#{self.author.email}"
  end

  def author_date_of_birth
    date=self.author.date_of_birth
    "#{date.strftime("#{date.day.ordinalize} of %B %Y")}"
  end

  def publisher_name
    "#{self.publisher.name}"
  end

  def publisher_address
    "#{self.publisher.address}"
  end
  
  def short_description 
    description.truncate(100)
  end

  def creation_date
    created_at.strftime("%d-%m-%Y")
  end


  

end
