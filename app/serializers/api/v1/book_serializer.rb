class Api::V1::BookSerializer < ActiveModel::Serializer 
  attributes :title, :isbn,:creation_date,:author_full_name,:publisher_name, :short_description,:author_email,:author_date_of_birth,:publisher_address
  belongs_to :author
  belongs_to :publisher

end
