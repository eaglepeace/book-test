class Api::V1::AuthorSerializer < ActiveModel::Serializer
  attributes  :first_name, :last_name, :email, :date_of_birth
  has_many :books
  has_many :publishers, through: :books

end
