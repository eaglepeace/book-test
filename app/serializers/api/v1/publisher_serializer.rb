class Api::V1::PublisherSerializer < ActiveModel::Serializer
  attributes :id, :name, :address
  has_many :authors, through: :books
end
