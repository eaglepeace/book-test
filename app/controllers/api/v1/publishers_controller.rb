class Api::V1::PublishersController < ApplicationController
  before_action :set_api_v1_publisher, only: [:show, :update, :destroy]

  # GET /api/v1/publishers
  def index
    @api_v1_publishers = Api::V1::Publisher.all

    render json: @api_v1_publishers
  end

  # GET /api/v1/publishers/1
  def show
    render json: @api_v1_publisher
  end

  # POST /api/v1/publishers
  def create
    @api_v1_publisher = Api::V1::Publisher.new(api_v1_publisher_params)

    if @api_v1_publisher.save
      render json: @api_v1_publisher, status: :created, location: @api_v1_publisher
    else
      render json: @api_v1_publisher.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /api/v1/publishers/1
  def update
    if @api_v1_publisher.update(api_v1_publisher_params)
      render json: @api_v1_publisher
    else
      render json: @api_v1_publisher.errors, status: :unprocessable_entity
    end
  end

  # DELETE /api/v1/publishers/1
  def destroy
    @api_v1_publisher.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_api_v1_publisher
      @api_v1_publisher = Api::V1::Publisher.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def api_v1_publisher_params
      params.require(:api_v1_publisher).permit(:name, :address)
    end
end
