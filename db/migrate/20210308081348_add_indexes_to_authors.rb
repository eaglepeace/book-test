class AddIndexesToAuthors < ActiveRecord::Migration[6.0]
  def up
    add_index(:api_v1_authors,[:email,:last_name])
  end
  def down
    remove_index(:api_v1_authors,[:email,:last_name])
  end
end
