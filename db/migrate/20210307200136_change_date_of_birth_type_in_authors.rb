class ChangeDateOfBirthTypeInAuthors < ActiveRecord::Migration[6.0]
  def up
    change_column :api_v1_authors, :date_of_birth, :date, :default => Date.current
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
  def down
    change_column :api_v1_authors, :date_of_birth, :string
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
