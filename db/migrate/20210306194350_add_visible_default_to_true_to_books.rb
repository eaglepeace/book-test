class AddVisibleDefaultToTrueToBooks < ActiveRecord::Migration[6.0]
  def change
    change_column :api_v1_books, :visible, :boolean, default: true
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
