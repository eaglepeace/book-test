class CreateApiV1Books < ActiveRecord::Migration[6.0]
  def change
    create_table :api_v1_books do |t|
      t.string :title
      t.string :description
      t.string :isbn
      t.integer :author_id, null: false
      t.integer :publisher_id, null: false
      t.boolean :visible

      t.timestamps
    end
  end
end
