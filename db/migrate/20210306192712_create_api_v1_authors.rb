class CreateApiV1Authors < ActiveRecord::Migration[6.0]
  def change
    create_table :api_v1_authors do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :date_of_birth

      t.timestamps
    end
  end
end
