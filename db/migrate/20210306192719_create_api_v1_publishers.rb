class CreateApiV1Publishers < ActiveRecord::Migration[6.0]
  def change
    create_table :api_v1_publishers do |t|
      t.string :name
      t.string :address

      t.timestamps
    end
  end
end
