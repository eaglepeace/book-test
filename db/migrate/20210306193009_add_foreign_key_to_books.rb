class AddForeignKeyToBooks < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :api_v1_books, :api_v1_authors, column: "author_id"
    add_foreign_key :api_v1_books, :api_v1_publishers, column: "publisher_id"
  end
end
