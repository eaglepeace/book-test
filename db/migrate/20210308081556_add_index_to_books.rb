class AddIndexToBooks < ActiveRecord::Migration[6.0]
  def up
    add_index(:api_v1_books,:title)
  end

  def down
    remove_index(:api_v1_books,:title)
  end
end
