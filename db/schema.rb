# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_08_081556) do

  create_table "api_v1_authors", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.date "date_of_birth", default: "2021-03-07"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email", "last_name"], name: "index_api_v1_authors_on_email_and_last_name"
  end

  create_table "api_v1_books", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "isbn"
    t.integer "author_id", null: false
    t.integer "publisher_id", null: false
    t.boolean "visible", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["title"], name: "index_api_v1_books_on_title"
  end

  create_table "api_v1_publishers", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "api_v1_books", "api_v1_authors", column: "author_id"
  add_foreign_key "api_v1_books", "api_v1_publishers", column: "publisher_id"
end
