# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

authors=Api::V1::Author.create([{
  "id": 1,
  "first_name": "Link",
  "last_name": "Ivell",
  "email": "livell0@istockphoto.com",
  "date_of_birth": "8/23/2020"
}, {
  "id": 2,
  "first_name": "Donica",
  "last_name": "Storch",
  "email": "dstorch1@who.int",
  "date_of_birth": "3/5/2020"
}, {
  "id": 3,
  "first_name": "Sallee",
  "last_name": "Jasik",
  "email": "sjasik2@i2i.jp",
  "date_of_birth": "1/11/2021"
}, {
  "id": 4,
  "first_name": "Shanta",
  "last_name": "Sayle",
  "email": "ssayle3@phoca.cz",
  "date_of_birth": "2/19/2021"
}, {
  "id": 5,
  "first_name": "Elle",
  "last_name": "Hurnell",
  "email": "ehurnell4@bluehost.com",
  "date_of_birth": "5/1/2020"
}, {
  "id": 6,
  "first_name": "Gillie",
  "last_name": "Baston",
  "email": "gbaston5@odnoklassniki.ru",
  "date_of_birth": "8/13/2020"
}, {
  "id": 7,
  "first_name": "Raff",
  "last_name": "Loughrey",
  "email": "rloughrey6@uol.com.br",
  "date_of_birth": "12/23/2020"
}, {
  "id": 8,
  "first_name": "Fair",
  "last_name": "Dust",
  "email": "fdust7@ask.com",
  "date_of_birth": "4/12/2020"
}, {
  "id": 9,
  "first_name": "Randolph",
  "last_name": "Raftery",
  "email": "rraftery8@marriott.com",
  "date_of_birth": "10/31/2020"
}, {
  "id": 10,
  "first_name": "Wyn",
  "last_name": "Millea",
  "email": "wmillea9@delicious.com",
  "date_of_birth": "10/6/2020"
}, {
  "id": 11,
  "first_name": "Isobel",
  "last_name": "Shepheard",
  "email": "ishephearda@xrea.com",
  "date_of_birth": "12/5/2020"
}, {
  "id": 12,
  "first_name": "Vere",
  "last_name": "Shackel",
  "email": "vshackelb@ycombinator.com",
  "date_of_birth": "1/22/2021"
}, {
  "id": 13,
  "first_name": "Anissa",
  "last_name": "Coxwell",
  "email": "acoxwellc@businessweek.com",
  "date_of_birth": "4/29/2020"
}, {
  "id": 14,
  "first_name": "Olga",
  "last_name": "Aleixo",
  "email": "oaleixod@pbs.org",
  "date_of_birth": "9/30/2020"
}])



publishers = Api::V1::Publisher.create([{
  "id": 1,
  "name": "Meembee",
  "address": "311 Melby Point"
}, {
  "id": 2,
  "name": "Yakijo",
  "address": "4 Colorado Road"
}, {
  "id": 3,
  "name": "Voonder",
  "address": "1 Prairieview Terrace"
}, {
  "id": 4,
  "name": "Ntags",
  "address": "90369 Parkside Center"
}, {
  "id": 5,
  "name": "Aibox",
  "address": "84 Arapahoe Court"
}, {
  "id": 6,
  "name": "Zoonoodle",
  "address": "42469 Comanche Street"
}, {
  "id": 7,
  "name": "Layo",
  "address": "893 Paget Circle"
}, {
  "id": 8,
  "name": "Voonix",
  "address": "6067 East Junction"
}, {
  "id": 9,
  "name": "Jetpulse",
  "address": "635 Riverside Alley"
}, {
  "id": 10,
  "name": "Jabbercube",
  "address": "8 Ramsey Alley"
}, {
  "id": 11,
  "name": "Zoozzy",
  "address": "4770 Moland Trail"
}, {
  "id": 12,
  "name": "Quimba",
  "address": "47200 American Ash Pass"
}, {
  "id": 13,
  "name": "Flashset",
  "address": "8 Buena Vista Junction"
}, {
  "id": 14,
  "name": "Skiptube",
  "address": "5928 Mcbride Terrace"
}, {
  "id": 15,
  "name": "Janyx",
  "address": "12 Florence Place"
}, {
  "id": 16,
  "name": "Gabtune",
  "address": "5 Doe Crossing Alley"
}, {
  "id": 17,
  "name": "Ooba",
  "address": "3 Dapin Point"
}, {
  "id": 18,
  "name": "Browsedrive",
  "address": "81 Blaine Junction"
}, {
  "id": 19,
  "name": "Dabfeed",
  "address": "381 Meadow Ridge Street"
}, {
  "id": 20,
  "name": "Dynabox",
  "address": "03 Harbort Road"
}, {
  "id": 21,
  "name": "Youfeed",
  "address": "57351 Oriole Hill"
}])


books = Api::V1::Book.create([{
  "id": 1,
  "isbn": "906181816-8",
  "title": "That's My Boy",
  "description": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.",
  "author_id": 1,
  "publisher_id": 1
}, {
  "id": 2,
  "isbn": "500703052-X",
  "title": "So Big!",
  "description": "In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.\n\nSuspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.",
  "author_id": 1,
  "publisher_id": 1
}, {
  "id": 3,
  "isbn": "143537334-0",
  "title": "Contagion",
  "description": "Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
  "author_id": 3,
  "publisher_id": 1
}, {
  "id": 4,
  "isbn": "686789536-0",
  "title": "Post Mortem",
  "description": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.",
  "author_id": 3,
  "publisher_id": 1
}, {
  "id": 5,
  "isbn": "423125550-4",
  "title": "Mighty Uke",
  "description": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.",
  "author_id": 1,
  "publisher_id": 1
}, {
  "id": 6,
  "isbn": "207832103-6",
  "title": "Naked Face, The",
  "description": "Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.",
  "author_id": 2,
  "publisher_id": 1
}, {
  "id": 7,
  "isbn": "747461858-3",
  "title": "Maniac",
  "description": "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.",
  "author_id": 2,
  "publisher_id": 1
}, {
  "id": 8,
  "isbn": "076050497-0",
  "title": "Attenberg",
  "description": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.",
  "author_id": 2,
  "publisher_id": 1
}, {
  "id": 9,
  "isbn": "377904051-4",
  "title": "Messengers, The",
  "description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.\n\nDuis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
  "author_id": 4,
  "publisher_id": 1
}, {
  "id": 10,
  "isbn": "592010159-8",
  "title": "Gore Vidal: The United States of Amnesia",
  "description": "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.\n\nMorbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.",
  "author_id": 4,
  "publisher_id": 2
}, {
  "id": 11,
  "isbn": "237528663-4",
  "title": "Jimi: All Is by My Side",
  "description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.\n\nDuis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
  "author_id": 3,
  "publisher_id": 2
}, {
  "id": 12,
  "isbn": "889839494-2",
  "title": "When Evening Falls on Bucharest or Metabolism",
  "description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.\n\nDuis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.",
  "author_id": 4,
  "publisher_id": 2
}, {
  "id": 13,
  "isbn": "983587901-X",
  "title": "Lumiere and Company (Lumière et compagnie)",
  "description": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.",
  "author_id": 5,
  "publisher_id": 2
}, {
  "id": 14,
  "isbn": "858556431-8",
  "title": "The Christmas Wish",
  "description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.\n\nDuis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.",
  "author_id": 5,
  "publisher_id": 2
}, {
  "id": 15,
  "isbn": "581724079-3",
  "title": "The Old Maid",
  "description": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
  "author_id": 2,
  "publisher_id": 3
}, {
  "id": 16,
  "isbn": "399561696-8",
  "title": "Food of Love (Manjar de Amor)",
  "description": "Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.",
  "author_id": 3,
  "publisher_id": 2
}, {
  "id": 17,
  "isbn": "484288185-2",
  "title": "Crank: High Voltage",
  "description": "Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.\n\nProin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.",
  "author_id": 5,
  "publisher_id": 6
}, {
  "id": 18,
  "isbn": "625305815-0",
  "title": "Crime and Punishment in Suburbia",
  "description": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.",
  "author_id": 6,
  "publisher_id": 6
}, {
  "id": 19,
  "isbn": "454151877-5",
  "title": "Lifeforce",
  "description": "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.",
  "author_id": 5,
  "publisher_id": 6
}, ])



