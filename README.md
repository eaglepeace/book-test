# README

run the following command:
bundle install
rails db:prepare
rails db:seed


*********************************************************
through the endpoint http://localhost:3000/api/v1/books
*********************************************************

1. List all visible books having a publishing company (publisher). Each element of this list should contain the following information:

● Title
● Description(Truncate at 100 characters followed by ellipsis) 
● ISBN
● Author’s full name

*********************************************************
through the endpoint http://localhost:3000/api/v1/authors
*********************************************************

2. we get 2 point in 1. In short through this endpoint will be possible to List all books, in particular,  alphabetically by author's last name and moreover, at the same time, with all the books ordered in descending order by using a numeric position attribute called book_num book_num will print the total amount of books of the author and it will decrease book by book

*********************************************************
through the endpoint http://localhost:3000/api/v1/books/1
*********************************************************
3. display a single book having the following attributes

● Title
● Description
● ISBN
● Creation date (e.g. 23/08/1987)
● Author’s full name
● Author’s email address
● Author’s date of birth (e.g. 23rd of December 1970)
● Publisher’s name
● Publisher’s address

for the below stuff I have used the default RESTFUL DRY procedure used by rails

3. Create a new author.
4. Create a new publisher.
5. Create a new book.
6. Update an existing book.
7. Delete an existing book.

